<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html>
<html>
<head>
  <title>Home Page</title>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/style.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/fonts/*">
<script src="${pageContext.request.contextPath}/static/js/jquery-3.2.1.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/index4style.css">
<style>  
  #small-font
{
	Font-size:18px;
}
<style>
limit {
  display: inline-block;
  position: relative;
  width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
</style>
</style>
</head>


<body background="${pageContext.request.contextPath}/static/images/background.jpg" class="img-responsive"  id="small-font">


<%@ include file="Header_UI.jsp" %>
  
  <div class="container" id="mainbody">
<center>
<img src="${pageContext.request.contextPath}/static/images/admission1.png" class="img-responsive">
</center>
</div>
<hr id="mainbody" style="border-color:DodgerBlue ;flex:1;height:5px">

      <!-- Image Carousel tab-pane start Tab-Pane for Home-->
     
       <div class="container">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
 <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
      <div class="row"  >
      <div class="col-md-8">
        <img src="${pageContext.request.contextPath}/static/images/sliderimage-1.jpg" alt="" style="width:100%;height:400px">
        </div>
        <div class="col-md-4" >
        <div class="text"  style="postion:absolute;right:0px;opacity:0.6;filter:alpha(opacity=60)">
         <h3 class="text-shadow" >Vianet Communication announce Dashin and Tihar offer in 2017</h3>
                <p class="text-shadow">Vianet Communications Pvt. Ltd  announce the Dashin and Tihar Offer in 2017. Full description and Order online</p>
               <button class="btn btn-primary">Read More</button>
                </div>
        </div>
        </div>
        </div>
         <div class="item">
         <div class="row">
         <div class="col-md-8">
        <img src="${pageContext.request.contextPath}/static/images/sliderimage-2.jpg" alt="" style="width:100%;height:400px">
        </div>
        <div class="col-md-4">
         <div class="text"  style="postion:absolute;right:0px;opacity:0.6;filter:alpha(opacity=60)">
        <h3 class="text-shadow">Addmission open</h3>
                <p class="text-shadow">Addmission open in Texas college<br/>Courses: BIT<br/>Bachelor of computer science</p>
                <button class="btn btn-primary">Read More</button>
                </div>
                </div>
      </div>
      </div>
    <div class="item">
    <div class="row">
    <div class="col-md-8" >
        <img src="${pageContext.request.contextPath}/static/images/sliderimage-3.png" alt="" style="width:100%;height:400px">
        </div>
        <div class="col-md-4" >
         <div class="text"  style="postion:absolute;right:0px;opacity:0.6;filter:alpha(opacity=60)">
        <h3 class="text-shadow">2017 Global Azure Bootcamp in Nepal</h3>
                <p class="text-shadow">
All around the world user groups and communities want to learn about Azure and Cloud Computing! On April 22, 2017, all communities will come together once again in the fifth great Global Azure Bootcamp event! Each user group will organize their own one day deep dive class on Azure the way they see fit and how it works for their members.</p>
               <button class="btn btn-primary">Read More</button>
                </div>
                </div>
      </div>
      </div>
    </div>
 <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

<div class="row" id="mainbody">
<div class="col-md-4">
<h3>Category:News</h3>
<hr style="border-color:DodgerBlue ;flex:1;height:5px">
<div class="panel" >
<div class="panel-heading"  style="background-color:DodgerBlue"><h3>Sanima Banks Extension counter at Bhaktapur District Office</h3>
</div>
<div class="panel-body ;small-font">
		<img src="${pageContext.request.contextPath}/static/images/news.jpg" class="img-responsive"><br/>
		Sanima Bank has opened its first extension counter at Bhaktapur District Office. This extension counter will provide service to collect revenue of Bhaktapur District Office. The extension counter was jointly inaugurated by Chief Administrative Officer of Bhaktapur district, Mr. Drona Pokharel and Sanima Bank’s, Chief Executive Officer Mr. Bhuvan Dahal amidst a ceremony. The bank has been able to extend the service from Mechi to Mahakali
		
		<button class="btn btn-primary"> <a href="${pageContext.request.contextPath}/Readmore">Read More</a></button>
	</div>
	<div class="panel-footer ;small-font">
		<span class="glyphicon glyphicon-user">Editor:rubina </span>
		<span class="	glyphicon glyphicon-calendar">date:2034/32/23</span>
	</div>
</div>
</div>
<div class="col-md-4">
<h3>Category:Events</h3>
<hr style="border-color:DodgerBlue ;flex:1;height:5px">
<div class="panel" >
<div class="panel-heading" style="background-color:DodgerBlue">
		<h3>npNOG -3 Conference and workshop at Chitwan</h3>
	</div>
	<div class="panel-body">
		<img src="${pageContext.request.contextPath}/static/images/events.jpg" class="img-responsive"><br/>
		Nepal Network Operators Group (npNOG), community of network operators who work in ISPs, Content Providers, Telcos and other areas of the Internet and ICT industry in Nepal will be organizing npNOG-3 event from December9th to December11th, 2017. The event will have 2 and halfdays workshops followed by a half-day conference at, Chitwan. The workshop will be conducted in three tracks, 1) Network Administration, 2) System Administration 3) IPv6
		
		<button class="btn btn-primary"><a href="">Read More</a></button>
	</div>
	<div class="panel-footer">
		<span class="glyphicon glyphicon-user">Editor:rubina </span>
		<span class="	glyphicon glyphicon-calendar">date:2034/32/23</span>
	</div>
</div>
</div>
<hr style="border-color:DodgerBlue ;flex:1;height:5px">
<div class="col-md-4">
<img alt="" src="${pageContext.request.contextPath}/static/images/mega.jpg" style="width="100px"; height=260px"><br><br>
		<img alt="" src="${pageContext.request.contextPath}/static/images/worldlink.gif" style="width="100px"; height=327px">
		
</div>
</div>
<!-- second row -->
<div class="row" id="mainbody">
<div class="col-md-4">
<h3>Category:Job</h3>
<hr style="border-color:DodgerBlue ;flex:1;height:5px">
 <div class="panel" >
	<div class="panel-heading" style="background-color:DodgerBlue">
		<h3>Apply for Digital Content Writer Jobs, topnepal.com</h3>
	</div>
	<div class="panel-body">
		<img src="${pageContext.request.contextPath}/static/images/job.jpg" class="img-responsive"><br/>
		TOP Nepal International is a Digital Web-Media agency collaboration with the different organization from USA, Portugal, and Thailand.Our mission is to Build Websites, Content Development, Apps Development, Software Development and Digital Marketing as well. Job Position: Editor No. of Vacancies: 3; Job Position: Co-Editor; No. of vacancies: 3 Job Position: WRITER No. of Vacancies: 5; Salary: Negotiable; Employment Type:
		<button class="btn btn-primary"><a href="">Read More</a></button>
	</div>
	<div class="panel-footer">
		<span class="glyphicon glyphicon-user">Editor:rubina </span>
		<span class="	glyphicon glyphicon-calendar">date:2034/32/23</span>
	</div>
</div>
</div>
<div class="col-md-4">
<h3>Category:Interview</h3>
<hr style="border-color:DodgerBlue ;flex:1;height:5px">
<div class="panel">
	<div class="panel-heading" style="background-color:DodgerBlue">
		<h3>Interview with Campus Director for Hult Prize, Gaurav Khatri</h3>
	</div>
	<div class="panel-body">
		<img src="${pageContext.request.contextPath}/static/images/interview.jpg" class="img-responsive"><br/>
		It i��s not everyday that you have an opportunity to change the world. If you have a business plan to solve the Energy Crisis, register soon.  What is HP at IOE Pulchowk ? Hult Prize is the world’s largest startup accelerator for social entrepreneurship with a winning prize of 1 Million USD. Each year students from all around the world are challenged on a key social issue, who then work to develop social business models that
		
		<button class="btn btn-primary"><a href="">Read More</a></button>
	</div>
	<div class="panel-footer">
		<span class="glyphicon glyphicon-user">Editor:rubina </span>
		<span class="	glyphicon glyphicon-calendar">date:2034/32/23</span>
	</div>
</div>
</div>
<hr style="border-color:DodgerBlue ;flex:1;height:5px">
<div class="col-md-4">
<img alt="" src="${pageContext.request.contextPath}/static/images/ads2.jpg"  class="img-responsive"style="width="300px"; height=260px"><br><br>
		<img alt="" src="${pageContext.request.contextPath}/static/images/ads3.jpg" class="img-responsive" style="width="100px"; height=327px">
		
</div>
</div>

<br/><br/>
<!-- Footer -->
	<%@ include file="footer_UI.jsp"%>
</body>
</html>