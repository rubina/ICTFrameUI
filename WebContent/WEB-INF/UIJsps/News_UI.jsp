<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

</head>
<body id="small-font" background="${pageContext.request.contextPath}/static/images/background.jpg">
<%@ include file="Header_UI.jsp"%>
<div class="row" id="mainbody">
	<div class="col-md-8" >
	<h3>Category:News</h3>
<hr style="border-color:DodgerBlue ;flex:1;height:5px">

<c:forEach var="row" items="${news}">
 <div class="panel"> 
 <div class="panel-header" style="background-color:DodgerBlue"> 
 <input type="hidden" name="id" value="${row.id}">
  <h3><a href="<c:url value='/ViewNewsDetails-${row.id}-news' />">   ${row.title}</a> </h3><br />
    </div>
    <div class="panel-body">
    <%-- image: ${row.image} --%>
    <img src="${pageContext.request.contextPath}/static/images/${row.photo.name}" class="img-responsive"><br/>
     ${row.description}<br /><a href="<c:url value='/ViewNewsDetails-${row.id}-news' />"><button class="btn btn-primary">ReadMore</button></a>
    </div>
    <div class="panel-footer">
    <span class="glyphicon glyphicon-user">editor:${row.editor}</span>
    <span class="	glyphicon glyphicon-calendar">Date:${row.date}</span>
      </div>
      
       <!-- comment -->
       <div class="row">
	<div class="col-sm-7">
	<div class="panel">
	<form method="POST" action="${pageContext.request.contextPath}/comment?id=${row.id}">
	<div class="panel-heading">
	<h2>Leave a comment below</h2>
	</div>
	<div class="panel-body">
	<span class="glyphicon glyphicon-user"></span> <input type="text" id="username" placeholder="your name"name="username" required><br/>
	<br/><textarea rows=3 cols=50 id="comment" name="comment" class="html-text-box"placeholder="Add Comment"></textarea>
	<button class="btn btn-primary">Comment</button>
	</form>
		</div>
	</div>
	</div>
	</div>
	<hr style="border-color:DodgerBlue ;flex:1;height:5px">
	 <div class="row" id="mainbody">
	<img src="${pageContext.request.contextPath}/static/images/userlogo.png" style="height:50px;width:50px"><b>${row.newscomment.username}</b>     <span class="glyphicon glyphicon-calendar"> </span>${row.newscomment.date}
	 <br/>
	 ${row.newscomment.comment }
      </div>
       </div>
     
      </c:forEach>
     
	</div>
	<div class="col-md-3"><img alt="" src="${pageContext.request.contextPath}/static/images/mega.jpg" style="width="100px"; height=260px"><br><br>
		<img alt="" src="${pageContext.request.contextPath}/static/images/worldlink.gif" style="width="100px"; height=327px">
	<br/><br/><h3>Catgory:Events</h3>
	<c:forEach var="data" items="${events}">
 <div class="panel"> 
 <div class="panel-header" style="background-color:DodgerBlue"> 
  <h3><a href="<c:url value='/ViewEventsDetails-${data.id}-events' />">${data.title} </a></h3><br />
    </div>
    <div class="panel-body">
    <%-- image: ${row.image} --%>
   <img src="${pageContext.request.contextPath}/static/images/${data.photo.name}" class="img-responsive"><br/>
   <a href="<c:url value='/ViewEventsDetails-${data.id}-events' />"><button class="btn btn-primary">ReadMore</button></a>
    </div>
    <div class="panel-footer">
    <span class="glyphicon glyphicon-user">editor:${data.editor}</span>
     <span class="glyphicon glyphicon-calendar">Date:${data.date}</span>
      </div>
       </div>
      </c:forEach>
		
		 </div>
	</div>
	
	
<%@ include file="footer_UI.jsp"%>
</body>
</html>