<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/style.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/fonts/*">
<script src="${pageContext.request.contextPath}/static/js/jquery-3.2.1.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/index4style.css">
<title>Courses</title>
<style>
div.one {
border: 2px solid #f2f2f2 ;
}
</style>
</head>
<body  id="small-font" background="${pageContext.request.contextPath}/static/images/background.jpg">
<%@ include file="Header_UI.jsp"%>
 
<nav class="navbar navbar-default" id ="nav" >
  <div class="container">
 <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav" id="nav-nav" >
       
         <li class="active"><a  data-toggle="tab" href="#TU">TU Affiliated<%-- <br/><br/><img src="${pageContext.request.contextPath}/static/images/tulogo.jpg" style="height:200px; width:200px"> --%></a></li>
        <li ><a data-toggle="tab"href="#PU">PU Affiliated<%-- <br/><br/><img src="${pageContext.request.contextPath}/static/images/pulogo.jpg" style="height:200px;width:200px"> --%></a></li>
        <li ><a data-toggle="tab" href="#KU">KU Affiliated<%-- <br/><br/><img src="${pageContext.request.contextPath}/static/images/kulogo.jpg" style="height:200px;width:200px"> --%></a></li>
        </ul>
      </div>
      </div>
    </nav>
  <!--   <hr id="mainbody"style="border-color:DodgerBlue ;flex:1;height:10px"> -->
   <%--  <div class="container" id="mainbody">
<center>
<img src="${pageContext.request.contextPath}/static/images/admission1.png" class="img-responsive">
</center>
</div><br/> --%>

    
    <!-- Tab pane for TU
 -->   
  <div class="tab-content" id="mainbody">
    <div id="TU" class="tab-pane fade in active">
    <div class="row" >
    <div class="col-md-4">
    <div class="one">
    <ul>
    <li><a data-toggle="tab" href="#BSCCSIT"><h3>BSC.CSIT<span class="glyphicon glyphicon-chevron-right"></span></h3>     </a></li>
    <li><a data-toggle="tab" href="#BIM"><h3>BIM <span class="glyphicon glyphicon-chevron-right"></span></h3></a></li>
   </ul>
    </div>
    <br/>
   <div class="row">
   <div class="col-md-1"><img alt="" src="${pageContext.request.contextPath}/static/images/mega.jpg" style="width="100px"; height=260px"><br><br>
		<img alt="" src="${pageContext.request.contextPath}/static/images/worldlink.gif" style="width="100px"; height=327px">
		 </div>
   </div>
    </div>
    <!-- ads -->
  
    
    
    <!-- Tab pane for BSC CSIT -->
    <div class="col-md-8">
   <div class="tab-content">
    <div id="BSCCSIT" class="tab-pane fade in active">
    
     <center>
     <img src="${pageContext.request.contextPath}/static/images/bscit.png" class="img-responsive">
     </center>
     <ul>
    <li> University: Tribhuvan University</li>
	<li>Duration: 4 years</li>
	<li>Academic Year: Semester</li>
	<li>Faculty: IT, computing studies and technology</li>
</ul>
 <p>Bachelors of Science in Computer Science and Information Technology (B.Sc.CSIT) is a four year course affiliated to Tribhuvan University designed to provide the student with all sorts of knowledge in the field of Information Technology and Computing.
<br/>The program involves, in addition to conventional lectures, a great deal of practical and project works. The program develops the underlying principles of both Computer Science and Information Technology and shows how these principles can be applied to real world problems. This program develops the skills that are essential for both computer professionals and IT specialists. 
<br/>The design and implementation of B.Sc.CSIT course offers new challenges when compared to the traditional computing environment. The recent emergence of global business, new technologies for data processing and data communication / networking environment, equip specialized science graduates to focus on professional careers in Information Technology. 
<br/>The B.Sc.CSIT program provides the students with adequate theoretical and practical knowledge which will enable students to effectively participate in solving the complex problem of the IT industry. </p>
  
  
  <h2> Eligibility:</h2>
The candidate applying for B.Sc.CSIT program:
<ul>
<li>Should have successfully completed a twelve year of schooling in the science stream or equivalent from any university, board or institution recognized by TU.</li>
<li>Should have secured a minimum of second division in their +2 or equivalent.</li>
<li>Should have successfully passed the entrance examination conducted by TU securing at least 35% marks.</li>
<li>Compiled with all the application procedures.</li>
</ul>
(Note: Both Biology and Mathematics group of students of +2 level are eligible to apply for the course) 


<h2>Entrance Examination:</h2>
<ul>
<li>The Entrance Examination will be of 100 Full Marks including the subjects of Intermediate/10+2 level or equivalent incorporating English, Mathematics, Physics and Chemistry subjects.</li>
<li> All the Questions of Entrance Examination will be Objective Type.</li>
<li>The Pass Marks of the Entrance Examination is 35 Marks.</li>
<li>The Examination Time Duration will be of 2 hours.</li>
<h2>Also</h2>
<li>Students do not need to fill up the TU Admission Form in more than one college. Form filled from one college will be valid for all colleges/campuses.</li>
<li>Students of Grade 12 Supplementary Exam can also fill up the TU Admission Form but such students should present the Academic Transcript of Passing 10+2/Intermediate level, otherwise the Entrance Examination Result of such students will not be published.</li>
</ul>


<h2>Curricular Structure:</h2>
B.Sc.CSIT program comprises of the following courses: 
<table class="table border=1px">
<tr style="background-color:DodgerBlue">
<th>Courses</th>
<th>credits hrs</th>
</tr>
<tr>
<td>Computer Science Core Courses</td>
<td>75</td>
</tr>
<tr>
<td>Natural Science Elective Courses</td>
<td>2</td>
</tr>
<tr>
<td>Mathematics Courses</td>
<td>12</td>
</tr>
<tr>
<td>English Courses</td>
<td>3</td>
</tr>
<tr>
<td>Social Science and Management Courses</td>
<td>6</td>
</tr>
<tr>
<td>Computer Science Elective Courses</td>
<td>15</td>
</tr>
<tr>
<td>Internship and Project</td>
<td>9</td>
</tr>
<tr>
<td>Total Credit Hours</td>
<td>126</td>
</tr>
</table>


<h2>Jobs Prospective:</h2>
The B.Sc.CSIT graduates have a prosperous career opportunity at different government, non-government, private and public organizations, like software companies, telecommunications, computer networking companies etc. especially as a: 
<ul>
<li>Software Developer</li>
<li>Web Developer</li>
<li>Network Administrator</li>
<li>Database Administrator</li>
<li>IT Manager/Officer</li>
<li>Cryptographer</li>
<li>Ergonomics Program Designer</li>
<li>System Analyst</li>
<li>Project Manager</li>
<li>Document Specialist</li>
<li>Information System Auditor</li>
<li>Artificial Intelligence Specialist</li>
<li>Technical Writer</li>
<li>Information System Manager</li>
<li>Database Operator</li>
</ul>

<h2>College</h2>
<ul>
<li> Swastik College,Bhaktapur, Contact: 01-6635174</li>
<li>Indreni College,Bharatpur, Chitwan, Contact: 056-525246, 9845023877</li>
<li>Samajik College,Bhaktapur, Contact:01-6638497, 9849054204</li>
<li> Soch college of IT, Pokhara,00977 61 540120</li>
<li>Tinau Technical College, Butwal, 071-560519,9841068274</li>
<li>Nist College, Banepa</li>
<li>Ambition Academy, Baneshwor, Contact: 4460442, 4464264</li>
</ul>
   
    </div>
    
     
  <!--   Tab-pane for BIM -->
    <div id="BIM" class="tab-pane fade">
    <h2>BIM(Bachelor in Information Management)</h2>
    <img src="${pageContext.request.contextPath}/static/images/bim.png" class="img-responsive"><br/><br/>
    BIM(Bachelor in Information Management) is a four-year bachelor's degree programme divided into eight semesters and 126 credit hours. It provides an interdisciplinary educational approach to students interested in the applied form of information and communication technology. BIM graduates gain knowledge and skills to develop business information systems, analyze and design databases, develop networks and internet applications and build business process plans through the use of information technology. 
    </div>
    </div>
    </div>
    </div>
     </div>
   
   
   
   
   <!-- Tab pane for PU -->
    <div id="PU" class="tab-pane fade">
    <div class="row" >
    <div class="col-md-4">
    <div class="one">
    <ul>
    <li><a data-toggle="tab" href="#BIT">BIT <span class="glyphicon glyphicon-chevron-right"></span></a>
    </ul>
    </div><br/>
    <div class="row">
   <div class="col-md-1"><img alt="" src="${pageContext.request.contextPath}/static/images/mega.jpg" style="width="100px"; height=260px"><br><br>
		<img alt="" src="${pageContext.request.contextPath}/static/images/worldlink.gif" style="width="100px"; height=327px">
		 </div>
   </div>
    </div>
    <div class="col-md-8">
   <div class="tab-content">
   <div class="one">
    <div id="BIT" class="tab-pane fade in active">
     <h2>BIT</h2>
      <ul>
    <li> University: Purbanchal  University</li>
	<li>Duration: 4 years</li>
	<li>Academic Year: Semester</li>
	<li>Faculty: IT, computing studies and technology</li>
</ul>
    <p>
    BIT in Nepal is one of best course in Nepal which directly deals with changing information technology. Over the years, it has been seen that BIT has been supplying the current need of IT industries. The bachelor in Information technology is an academic program comprising of core and advance IT unit. The core unit provides fundamentals of information technology that provides student the knowledge and skills in programming, system design, computer networks and communication. The advanced unit enables the students to exercise, develop and apply their knowledge and skills in many areas like multimedia, Artificial intelligence, mobile communication internet etc
    </p>
    
   <h2> Eligibility:</h2>
The candidate applying for this course should have successfully passed +2 in Science / Management / Humanities or equivalent degree with at least 45% in aggregate and 100 marks Mathematics paper.
   
  
   <h2>Jobs Prospective:</h2>
   <ul>
   <li>Analyst/Programmer</li>
	<li>IT Manager</li>
	<li>System Programmer</li>
	<li>System Analyst/ Designer</li>
	<li>Interactive Digital Media Specialist</li>
	<li>Web Administrator/ Developer</li>
	<li>Software Engineering</li>
	<li>Data Communication Engineering</li>
	<li>Database Administrator</li>
	<li>Network Administrator</li>
	<li>System Manager</li>
</ul>

<h2>Colleges</h2>
<ul>
<li>College of Information technology and engineering, Kathmandu</li>
<li>Kantipur City College, Kathmandu</li>
<li>White House Institute of Science and Technology</li>
<li>KIST college</li>
<li>Aryan School of Engineering</li>
<li>Asian Institute of Technology and Management</li>
</ul>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    
    </div>
    <br/>
    <%@ include file="footer_UI.jsp"%>
</body>
</html>