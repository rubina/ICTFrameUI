<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/static/css/style.css">
	<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/static/css/index4style.css">

</head>
<body id="small-font" background="${pageContext.request.contextPath}/static/images/background.jpg">
<%@ include file="Header_UI.jsp"%>
 <div class="row" id="mainbody">
	<div class="col-sm-8">
	<form method="POST">
 <div class="panel"> 
 <div class="panel-header" style="background-color:DodgerBlue"> 
  <h2>   ${events.title} </h2><br />
    </div>
    <div class="panel-body">
    <%-- image: ${events.image} --%>
    <img src="${pageContext.request.contextPath}/static/images/${events.photo.name}" class="img-responsive"><br/><br/>
     ${events.description}<br />
    </div>
    <div class="panel-footer">
    <span class="glyphicon glyphicon-user">editor:${events.editor}</span>
     Date: ${events.date}
      </div>
       </div>
      </form>
   	</div>
   	<div class="col-sm-3">
   	<c:forEach var="row" items="${events}">
 <div class="panel"> 
 <div class="panel-header" style="background-color:DodgerBlue"> 
  <h3><a href="<c:url value='/ViewEventsDetails-${row.id}-events' />">${row.title} </a></h3><br />
    </div>
    <div class="panel-body">
    <%-- image: ${row.image} --%>
   <img src="${pageContext.request.contextPath}/static/images/${row.photo.name}" class="img-responsive"><br/>
   <a href="<c:url value='/ViewEventsDetails-${row.id}-events' />"><button class="btn btn-primary">ReadMore</button></a>
    </div>
    <div class="panel-footer">
    <span class="glyphicon glyphicon-user">editor:${row.editor}</span>
     <span class="glyphicon glyphicon-calendar">Date:${row.date}</span>
      </div>
       </div>
      </c:forEach>
   	</div>
   	
	</div>
	<%@ include file="footer_UI.jsp" %>
</body>
</html>

