<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/static/css/style.css">
	<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/static/css/index4style.css">

</head>
<body id="small-font" background="${pageContext.request.contextPath}/static/images/background.jpg">
<%@ include file="Header_UI.jsp"%>
 <div class="row" id="mainbody">
	<div class="col-sm-8">
	<form method="POST">
 <div class="panel"> 
 <div class="panel-header" style="background-color:DimGray"> 
  <h2>   ${blog.title} </h2><br />
    </div>
    <div class="panel-body">
    <%-- image: ${blog.image} --%>
   <img src="${pageContext.request.contextPath}/static/images/${blog.photo.name}" class="img-responsive"><br/>
     ${blog.description}<br />
    </div>
    <div class="panel-footer">
    <span class="glyphicon glyphicon-user">editor:${blog.editor}</span>
     Date: ${blog.date}
      </div>
       </div>
       </form>
   	</div>
   
	</div>
	<%@ include file="footer_UI.jsp" %>
</body>
</html>

