<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/index4style.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Footer</title>
<style>
.fa {
padding:20px;
  font-size: 30px;
  width: 50px;
  text-align: center;
  text-decoration: none;
  margin: 5px 2px;
}

.fa:hover {
    opacity: 0.7;
}

.fa-facebook {
  background: #3B5998;
  color: white;
}

.fa-twitter {
  background: #55ACEE;
  color: white;
}

.fa-google {
  background: #dd4b39;
  color: white;
}

.fa-linkedin {
  background: #007bb5;
  color: white;
}

.fa-youtube {
  background: #bb0000;
  color: white;
}
</style>
</head>
<body>
<footer id="mainbody" style="bottom:0px;position:relative;overflow:hidden;">
<div class="container"  style="background-color:#190608 " >
 
  <div class="col-md-4 text-center heading">
      <h3 class="about-us text-center"> 
        <div class="divider "> ABOUT US</div> <p> <br>Softech foundation New Baneswor-10, Kathmandu, Nepal <br>Registration No. 20/073-074 <br>Executive Director Sanjay rauniyar. </p> </h3>

          <div class="abt-one text-center">

</div>
</div>
<div class="col-md-4">
<div class="container" style="bottom:0px">
<a href="https://www.facebook.com/" class="fa fa-facebook"></a>
<a href="#" class="fa fa-twitter"></a>
<a href="#" class="fa fa-youtube"></a>
<a href="#" class="fa fa-google"></a>
<a href="#" class="fa fa-linkedin"></a>

</div>
  </div>
<div class="col-md-4 text-center heading">
      <h3 class="about-us text-center"> 
        <div class="divider"> CONTACT US </div> <p> <br> Softech foundation 
New Baneswor-10, Kathmandu, Nepal<br>Email: info@ictframe.com <br>
Phone: +977-9851051578, 9818525091 </p> </h3>
          <div class="abt-one text-center">
</div>
  </div>
  </div>
  
</body>
</html>