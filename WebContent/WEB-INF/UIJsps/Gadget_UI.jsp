<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body background="${pageContext.request.contextPath}/static/images/background.jpg"  id="small-font">
<%@ include file="Header_UI.jsp"%>
<div class="row" id="mainbody">
	<div class="col-md-8" >
	<h3>Category:Gadgets</h3>
<hr style="border-color:DodgerBlue ;flex:1;height:5px">
<c:forEach var="row" items="${gadget}">
 <div class="panel"> 
 <div class="panel-header" style="background-color:DodgerBlue"> 
  <h3>  <a href="<c:url value='/ViewGadgetsDetails-${row.id}-gadget' />"> ${row.title}</a> </h3><br />
    </div>
    <div class="panel-body">
    <%-- image: ${row.image} --%>
   <img src="${pageContext.request.contextPath}/static/images/${row.photo.name}" class="img-responsive"><br/>
     ${row.description}<br /><a href="<c:url value='/ViewGadgetsDetails-${row.id}-gadget' />"><button class="btn btn-primary">ReadMore</button></a>
    </div>
    <div class="panel-footer">
    <span class="glyphicon glyphicon-user">editor:${row.editor}</span>
   <span class="	glyphicon glyphicon-calendar">  Date:${row.date}</span>
      </div>
       </div>
      </c:forEach>
	</div>
	<div class="col-md-3"><img alt="" src="${pageContext.request.contextPath}/static/images/mega.jpg" style="width="100px"; height=260px"><br><br>
		<img alt="" src="${pageContext.request.contextPath}/static/images/worldlink.gif" style="width="100px"; height=327px">
		<br/><br/>
		<c:forEach var="row" items="${gadget}">
 <div class="panel"> 
 <div class="panel-header" style="background-color:DodgerBlue"> 
  <h3><a href="<c:url value='/ViewEventsDetails-${row.id}-events' />">${row.title} </a></h3><br />
    </div>
    <div class="panel-body">
    <%-- image: ${row.image} --%>
   <img src="${pageContext.request.contextPath}/static/images/${row.photo.name}" class="img-responsive"><br/>
   <a href="<c:url value='/ViewEventsDetails-${row.id}-events' />"><button class="btn btn-primary">ReadMore</button></a>
    </div>
    <div class="panel-footer">
    <span class="glyphicon glyphicon-user">editor:${row.editor}</span>
     <span class="glyphicon glyphicon-calendar">Date:${row.date}</span>
      </div>
       </div>
      </c:forEach> </div>
	</div>
<%@ include file="footer_UI.jsp"%>
</body>
</html>