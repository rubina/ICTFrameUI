<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html>
<html>
<head>
  <title> Trail</title>
 <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/style.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/styles3.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/fonts/*">
<script src="${pageContext.request.contextPath}/static/js/jquery-3.2.1.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/index4style.css">
<style>  
  #small-font
{
	Font-size:18px;
}
</style>
</head>


<body id="mainbody">
<header>
  <div class="header-top ">
    <div class="container">
      <div class="head-main pull-left">
        <a href="#">
          <img src="${pageContext.request.contextPath}/static/images/logo.png" alt="" width="150px" height="100px">
        </a>
      </div>
      
    <br/>
  <!--  <div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-3 pull-right">
            <form action="search" class="search-form">
                <div class="form-group has-feedback">
                <label for="search" class="sr-only">Search</label>
                <input type="text" class="form-control" name="search"  id="search" placeholder="search by title">
                 <span class="glyphicon glyphicon-search form-control-feedback"></span>
              </div>
            </form>
        </div>
    </div>
    </div> -->
    <div class="container">
<div class="row">
<div class="col-md-4 col-md-offset-3 pull-right">
 <nav class="navbar navbar-default">
        <div class="nav nav-justified navbar-nav">
<form class="navbar-form navbar-search" role="search">
                <div class="input-group">
                 <div class="input-group-btn">
                        <button type="button" placeholder="search by"class="btn btn-search btn-default dropdown-toggle" data-toggle="dropdown">
                            <span class="glyphicon glyphicon-search"></span>
                            <span class="label-icon">Search By</span>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-left" role="menu">
                           <li>
                                <a href="#">
                                    <span class="glyphicon glyphicon-user"></span>
                                    <span class="label-icon"> News</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                <span class="glyphicon glyphicon-book"></span>
                                <span class="label-icon">Events</span>
                                </a>
                            </li>
                              <li>
                                <a href="#">
                                <span class="glyphicon glyphicon-book"></span>
                                <span class="label-icon">Job</span>
                                </a>
                            </li>
                            
                              <li>
                                <a href="#">
                                <span class="glyphicon glyphicon-book"></span>
                                <span class="label-icon">Interview</span>
                                </a>
                            </li>
                            
                              <li>
                                <a href="#">
                                <span class="glyphicon glyphicon-book"></span>
                                <span class="label-icon">Blog</span>
                                </a>
                            </li>
                            
                              <li>
                                <a href="#">
                                <span class="glyphicon glyphicon-book"></span>
                                <span class="label-icon">Gadgets</span>
                                </a>
                            </li>
                            
                              <li>
                                <a href="#">
                                <span class="glyphicon glyphicon-book"></span>
                                <span class="label-icon">Video</span>
                                </a>
                            </li>
                            
                        </ul>
                    </div>
<input type="text" class="form-control">
                 <div class="input-group-btn">
                        <button type="button" class="btn btn-search btn-default">
                        <a href="search.jsp" >GO</a>
                        </button>
                    </div>
                </div>  
            </form>   
         
        </div>
    </nav>
    </div>
    </div>
    </div>
</div></div>
  </header>

    
  <br/><br/>

  <nav class="navbar navbar-inverse" id ="nav" >
  <div class="container">
   <!--  <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      
    </div> -->
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav" id="nav-nav" >
        <li class="active"><a href="home"><span class="glyphicon glyphicon-home"></span></a></li>
        
        <li id="small-font"><a href="News">News</a></li>
        <li id="small-font"><a href="Events">Events</a></li>
        <li id="small-font"><a href="Job">Job</a></li>
        <li id="small-font"><a href="Interview">Interview</a></li>
        <li id="small-font"><a href="Blogs">Blog</a></li>
        <li id="small-font"><a  href="Gadgets">Gadget</a></li>
        <li id="small-font"><a  href="Video">Video</a></li>
        <li id="small-font"> <a href="Courses">Courses</a></li>
      </ul>
      
    </div>
  </div>
</nav>
<!-- <hr id="mainbody" style="border-color:DodgerBlue ;flex:1;height:5px"> -->
</body>
<script>
$(function(){
    
    $(".input-group-btn .dropdown-menu li a").click(function(){

        var selText = $(this).html();
    
        //working version - for single button //
       //$('.btn:first-child').html(selText+'<span class="caret"></span>');  
       
       //working version - for multiple buttons //
       $(this).parents('.input-group-btn').find('.btn-search').html(selText);

   });

});
</script>
</html>