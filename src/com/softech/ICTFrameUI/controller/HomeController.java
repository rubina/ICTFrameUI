package com.softech.ICTFrameUI.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class HomeController {
	
	
	@RequestMapping("/admin")
	public String showHome()
	{
		return "admin";
		
	}
	@RequestMapping("/AdminDashboard")
	public String showDashboard()
	{
				return "AdminDashboard";
		
	}
	
	
	@RequestMapping("/")
	public String showIndex()
	{
		return "Index_UI";
	}
	@RequestMapping("/home")
	public String showIndexPage()
	{
		return "Index_UI";
	}
	@RequestMapping("Courses")
	public String showCourses()
	{
		return "Courses_UI";
	}
	/*@RequestMapping("News")
	public String showNewsPage()
	{
		
		return "News";
	}*/
	
	@RequestMapping("/search")
	public String showsearchresult(HttpServletRequest request,Model model,@RequestParam("search") String item)
	{
		String searchElement=request.getParameter("search");
		return "";
	}

}
