package com.softech.ICTFrameUI.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.softech.ICTFrameUI.services.JobService;
import com.softech.ICTFrameUI.DAOModels.Job;


@Controller
public class JobController {
	@Autowired
	private JobService jobservice;
	public void setJobService(JobService job)
	{
		jobservice=job;
	}
	@RequestMapping("/Job")
	public String showEventList(Model model)
	{
	List<Job> job=jobservice.GetAllJob();
	model.addAttribute("job", job);
	return "Job_UI";
	}
	
	@RequestMapping(value = { "/ViewJobDetails-{id}-job" }, method = RequestMethod.GET)
		public String showJobDetails(Model model,@PathVariable int id,Job job)
	{
		job=jobservice.GetSpecificJob(id);
		model.addAttribute("job",job);
		return "JobDetails_UI";
	}
	
	
	

}
