package com.softech.ICTFrameUI.controller;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.softech.ICTFrameUI.services.EventsService;
import com.softech.ICTFrameUI.services.NewsService;
import com.softech.ICTFrameUI.DAOModels.Events;
import com.softech.ICTFrameUI.DAOModels.News;
import com.softech.ICTFrameUI.DAOModels.NewsComment;

@Controller
public class NewsController {
	@Autowired
	private NewsService newsservice;
	public void setNewsService(NewsService news)
	{
		newsservice=news;
	}
	private EventsService eventservice;
	public void setEventService(EventsService events)
	{
		eventservice=events;
	}
	
	@RequestMapping("/News")
	public String showNewsList(Model model)
	{
	List<News> news=newsservice.GetAllNewes();
	model.addAttribute("news", news);
	return "News_UI";
	}
	
	@RequestMapping(value = { "/ViewNewsDetails-{id}-news" }, method = RequestMethod.GET)
		public String showNewsDetails(Model model,@PathVariable int id,News news)
	{
		news=newsservice.GetSpecificNews(id);
		model.addAttribute("news",news);
		List<Events> events=eventservice.GetAllEvents();
		model.addAttribute("events", events);
		return "NewsDetails_UI";
	}
	@RequestMapping(value = "/comment")
	public String uploadBlogphoto(HttpServletRequest request,
		 @RequestParam("id") int id, Model model, News news) throws Exception, IOException {

		
		News persistentnews = newsservice.GetSpecificNews(id);
		String username=request.getParameter("username");
		String comment=request.getParameter("comment");
		System.out.println(username+comment);
		NewsComment newscomment = new NewsComment(username, comment);
		persistentnews.setNewscomment(newscomment);
		
		newsservice.updatecomment(persistentnews);

		return "redirect:/News";
	}
	
	
	
}
