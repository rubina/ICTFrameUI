package com.softech.ICTFrameUI.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.softech.ICTFrameUI.services.InterviewService;
import com.softech.ICTFrameUI.DAOModels.Interview;


@Controller
public class InterviewController {
	@Autowired
	private InterviewService interviewservice;
	public void setGadgetService(InterviewService interview)
	{
		interviewservice=interview;	
	}
	
	@RequestMapping("/Interview")
	public String showEventList(Model model)
	{
	List<Interview> interview=interviewservice.GetAllInterview();
	model.addAttribute("interview", interview);
	return "Interview_UI";
	}
	
	@RequestMapping(value = { "/ViewInterviewDetails-{id}-interview" }, method = RequestMethod.GET)
		public String showInterviewDetails(Model model,@PathVariable int id,Interview interview)
	{
		interview=interviewservice.GetSpecificInterview(id);
		model.addAttribute("interview",interview);
		return "InterviewDetails_UI";
	}
}
