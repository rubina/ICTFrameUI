package com.softech.ICTFrameUI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.softech.ICTFrameUI.services.BlogService;
import com.softech.ICTFrameUI.DAOModels.Blog;
@Controller
public class BlogController {
	@Autowired
	private BlogService blogservice;
	public void setBLogService(BlogService blog)
	{
		blogservice=blog;	
	}
	
	@RequestMapping("/Blogs")
	public String showBlogList(Model model)
	{
	List<Blog> blog=blogservice.GetAllBlog();
	model.addAttribute("blog", blog);
	return "Blog_UI";
	}
	
	@RequestMapping(value = { "/ViewBlogDetails-{id}-blog" }, method = RequestMethod.GET)
		public String showBlogDetails(Model model,@PathVariable int id,Blog blog)
	{
		blog=blogservice.GetSpecificBlog(id);
		model.addAttribute("blog",blog);
		return "BlogDetails_UI";
	}
}
