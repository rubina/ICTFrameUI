package com.softech.ICTFrameUI.controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.softech.ICTFrameUI.services.EventsService;
import com.softech.ICTFrameUI.DAOModels.Events;

@Controller
public class EventsController {
	@Autowired
	private EventsService eventservice;
	public void setEventService(EventsService event)
	{
	eventservice=event;	
	}
	
	@RequestMapping("/Events")
	public String showEventList(Model model)
	{
	List<Events> event=eventservice.GetAllEvents();
	model.addAttribute("events", event);
	return "Events_UI";
	}
	
	@RequestMapping(value = { "/ViewEventsDetails-{id}-events" }, method = RequestMethod.GET)
		public String showBlogDetails(Model model,@PathVariable int id,Events event)
	{
		event=eventservice.GetSpecificEvents(id);
		model.addAttribute("events",event);
		return "EventsDetails_UI";
	}

}
