package com.softech.ICTFrameUI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.softech.ICTFrameUI.services.VideoService;
import com.softech.ICTFrameUI.DAOModels.Video;



@Controller
public class VideoController {
	@Autowired
	private VideoService videoservice;
	public void setVideoService(VideoService video)
	{
		videoservice=video;
	}
	
	@RequestMapping("/Video")
	public String showVideoList(Model model)
	{
	List<Video> video=videoservice.GetAllVideo();
	model.addAttribute("video", video);
	return "Video_UI";
	}
	
	@RequestMapping(value = { "/ViewVideoDetails-{id}-video" }, method = RequestMethod.GET)
		public String showNewsDetails(Model model,@PathVariable int id,Video video)
	{
		video=videoservice.GetSpecificVideo(id);
		model.addAttribute("video",video);
		return "VideoDetails_UI";
	}
	
}
