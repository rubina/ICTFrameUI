package com.softech.ICTFrameUI.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.softech.ICTFrameUI.services.GadgetService;
import com.softech.ICTFrameUI.DAOModels.Gadgets;
@Controller
public class GadgetController {
	@Autowired
	private GadgetService gadgetservice;
	public void setGadgetService(GadgetService gadget)
	{
		gadgetservice=gadget;	
	}
	
	@RequestMapping("/Gadgets")
	public String showEventList(Model model)
	{
	List<Gadgets> gadgets =gadgetservice.GetAllGadgets();
	model.addAttribute("gadget", gadgets);
	return "Gadget_UI";
	}
	
	@RequestMapping(value = { "/ViewGadgetsDetails-{id}-gadget" }, method = RequestMethod.GET)
		public String showBlogDetails(Model model,@PathVariable int id,Gadgets gadget)
	{
		gadget=gadgetservice.GetSpecificGadgets(id);
		model.addAttribute("gadget",gadget);
		return "GadgetsDetails_UI";
	}

}
