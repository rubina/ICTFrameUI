package com.softech.ICTFrameUI.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.softech.ICTFrameUI.DAOModels.Blog;

@Transactional
@Component
public class BlogDAO {
	@Autowired
	private SessionFactory sessionFactory;
	public Session session()
	{
		return sessionFactory.getCurrentSession();
	}
	public void SaveBlog(Blog blog)
	{
		Transaction trans=session().beginTransaction();
		session().save(blog);
		trans.commit();
	}
	public List<Blog> GetAllBlog()
	{
		Transaction trans=session().beginTransaction();
		List<Blog> blog=session().createQuery("from Blog B ORDER BY B.id DESC").list();
		trans.commit();
		return blog;
	}
	
	public Blog GetSpecificBlog(int id)
	{
		Transaction trans=session().beginTransaction();
		Blog blog=(Blog)session().get(Blog.class,id);
		trans.commit();
		return blog;
	}
	
}
