package com.softech.ICTFrameUI.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.softech.ICTFrameUI.DAOModels.Video;
@Transactional
@Component
public class VideoDAO {
	@Autowired 
	private SessionFactory sessionFactory;
	public Session session()
	{
		return sessionFactory.getCurrentSession();
	}
	public void SaveVideo(Video video)
	{
		Transaction trans=session().beginTransaction();
		session().save(video);
		trans.commit();
	}
	public List<Video> GetAllVideo()
	{
		Transaction trans=session().beginTransaction();
		List<Video> video=session().createQuery("from Video V ORDER BY V.id DESC").list();
		trans.commit();
		return video;
	}
	public Video GetSpecificVideo(int id)
	{
		Transaction trans=session().beginTransaction();
		Video video=(Video) session().get(Video.class,id);
		trans.commit();
		return video;
	}
}
