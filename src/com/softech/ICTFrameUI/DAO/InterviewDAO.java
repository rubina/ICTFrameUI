package com.softech.ICTFrameUI.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.softech.ICTFrameUI.DAOModels.Interview;

@Transactional 
@Component
public class InterviewDAO {


	@Autowired
	private SessionFactory sessionFactory;
	public Session session()
	{
		return sessionFactory.getCurrentSession();
	}
	public void SaveInterview(Interview interview)
	{
		Transaction trans=session().beginTransaction();
		session().save(interview);
		trans.commit();
	}
	public List<Interview> GetAllInterview()
	{
		Transaction trans=session().beginTransaction();
		List<Interview> interview=session().createQuery("from Interview I ORDER BY I.id DESC").list();
		trans.commit();
		return interview;
	}
	
	public Interview GetSpecificInterview(int id)
	{
		Transaction trans=session().beginTransaction();
		Interview interview=(Interview)session().get(Interview.class,id);
		trans.commit();
		return interview;
	}
	public void UpdateInterview(Interview interview)
	{
		Transaction trans=session().beginTransaction();
		session().saveOrUpdate(interview);
		trans.commit();
		
	}

}
