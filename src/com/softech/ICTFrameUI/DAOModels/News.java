package com.softech.ICTFrameUI.DAOModels;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="News")
public class News {
	@Id
	@GeneratedValue
	@Column(name="news_id")
	private int id;
	@Column(name="news_title")
	private String title;
	@Column(name="news_description",columnDefinition="LONGTEXT")
	private String description;
	@Column(name="news_date",columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
	private String date;
	@Column(name="news_editor")
	private String editor;
	
	@OneToOne(cascade = CascadeType.ALL)
	private NewsPhoto photo;
	@OneToOne(cascade = CascadeType.ALL)
	private NewsComment newscomment;
	public News()
	{}
	public News(String title,String description,String date,String editor)
	{
		this.title=title;
		this.description=description;
		this.date=date;
		this.editor=editor;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getEditor() {
		return editor;
	}
	public void setEditor(String editor) {
		this.editor = editor;
	}
	public NewsPhoto getPhoto() {
		return photo;
	}
	public void setPhoto(NewsPhoto photo) {
		this.photo = photo;
	}
	public NewsComment getNewscomment() {
		return newscomment;
	}
	public void setNewscomment(NewsComment newscomment) {
		this.newscomment = newscomment;
	}	
	
}
