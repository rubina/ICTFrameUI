package com.softech.ICTFrameUI.DAOModels;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Video")
public class Video {
	@Id
	@GeneratedValue
	@Column(name="video_id")
	private int id;
	@Column(name="video_title")
	private String title;
	@Column(name="video_link")
	private String link;
	@Column(name="video_description",columnDefinition="LONGTEXT")
	private String description;
	@Column(name="video_date",columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
	private String date;
	@Column(name="video_editor")
	private String editor;
public Video()
{}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public String getLink() {
	return link;
}
public void setLink(String link) {
	this.link = link;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public String getDate() {
	return date;
}
public void setDate(String date) {
	this.date = date;
}
public String getEditor() {
	return editor;
}
public void setEditor(String editor) {
	this.editor = editor;
}
}
