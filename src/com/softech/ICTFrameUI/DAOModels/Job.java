package com.softech.ICTFrameUI.DAOModels;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Job")
public class Job {
	@Id
	@GeneratedValue
	@Column(name="job_id")
	private int id;
	@Column(name="job_title")
	private String title;
	@Column(name="job_description",columnDefinition="LONGTEXT")
	private String description;
	@Column(name="job_date",columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
	private String date;
	@Column(name="job_editor")
	private String editor;
	
	@OneToOne(cascade = CascadeType.ALL)
	private JobPhoto photo;
	public Job()
	{
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getEditor() {
		return editor;
	}
	public void setEditor(String editor) {
		this.editor = editor;
	}
	public JobPhoto getPhoto() {
		return photo;
	}
	public void setPhoto(JobPhoto photo) {
		this.photo = photo;
	}
	

}
