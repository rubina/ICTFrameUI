package com.softech.ICTFrameUI.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softech.ICTFrameUI.DAO.EventsDAO;
import com.softech.ICTFrameUI.DAOModels.Events;
@Service("eventservice")
public class EventsService {
	@Autowired
	private EventsDAO eventsDAO;
	public void setEventsDAO(EventsDAO eventssdao)
	{
		this.eventsDAO=eventssdao;
	}
	public void SaveEvents(Events events)
	{
		eventsDAO.SaveEvents(events);
	}
	public List<Events> GetAllEvents()
	{
		return eventsDAO.GetAllEvents();
	}
	
	public Events GetSpecificEvents(int id)
	{
		return eventsDAO.GetSpecificEvents(id);
	}
	public void UpdateEvents(Events events ,int id)
	{
		Events persistEvents=eventsDAO.GetSpecificEvents(id);
		persistEvents.setTitle(events.getTitle());
		persistEvents.setDescription(events.getDescription());
		persistEvents.setDate(events.getDate());
		persistEvents.setEditor(events.getEditor());
		eventsDAO.UpdateEvents(persistEvents);
	}
		
}
