package com.softech.ICTFrameUI.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softech.ICTFrameUI.DAO.NewsDAO;
import com.softech.ICTFrameUI.DAOModels.News;

@Service("newsservice")
public class NewsService {
	@Autowired
	private NewsDAO newsdao;
	public void setNewsDAO(NewsDAO newssdao)
	{
		this.newsdao=newssdao;
	}
	public void SaveNews(News news)
	{
		newsdao.SaveNews(news);
	}
	public List<News> GetAllNewes()
	{
		return newsdao.GetAllNews();
	}
	public News GetSpecificNews(int id)
	{
		return newsdao.GetSpecificNews(id);
	}
	public void UpdateNews(News news,int id)
	{
		News persistNews=newsdao.GetSpecificNews(id);
		persistNews.setTitle(news.getTitle());
		persistNews.setDescription(news.getDescription());
		persistNews.setDate(news.getDate());
		persistNews.setEditor(news.getEditor());
		newsdao.UpdateNews(persistNews);
	}
	
	public void updatecomment(News news)
	{
		newsdao.UpdateNews(news);
	}

}
