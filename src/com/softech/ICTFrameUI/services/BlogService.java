package com.softech.ICTFrameUI.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softech.ICTFrameUI.DAO.BlogDAO;
import com.softech.ICTFrameUI.DAOModels.Blog;
@Service("blogservice")
public class BlogService {
	@Autowired
	private BlogDAO BlogDAO;
	public void setBlogDAO(BlogDAO Blogsdao)
	{
		this.BlogDAO=Blogsdao;
	}
	public void SaveBlog(Blog blog)
	{
		BlogDAO.SaveBlog(blog);
	}
	public List<Blog> GetAllBlog()
	{
		return BlogDAO.GetAllBlog();
	}
	
	public Blog GetSpecificBlog(int id)
	{
		return BlogDAO.GetSpecificBlog(id);
	}
	
	

}
