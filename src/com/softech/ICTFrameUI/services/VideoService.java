package com.softech.ICTFrameUI.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softech.ICTFrameUI.DAO.VideoDAO;
import com.softech.ICTFrameUI.DAOModels.Video;


@Service
public class VideoService {
	@Autowired
	private VideoDAO videodao;
	public void setVideoDAO(VideoDAO videodao)
	{
		this.videodao=videodao;
	}
	public void SaveVideo(Video video)
	{
		videodao.SaveVideo(video);
	}
	public List<Video> GetAllVideo()
	{
		return videodao.GetAllVideo();
	}
	public Video GetSpecificVideo(int id)
	{
		return videodao.GetSpecificVideo(id);
	}
}
